# Analyzing CloudTrail with Athena


## Creating a table and partitioning data

First, open Athena in the Management Console. Next, double check if you have switched to the region of the S3 bucket containing the CloudTrail logs to avoid unnecessary data transfer costs. Afterward, execute the **creating_a_table_and_partitioning_data.sql** to create a table. Replace <BUCKET> with the name of your S3 bucket.

## Add Partitions
To be able to query data from your table, we need to add partitions. By executing **add_partitions_to_the_table.sql** for each partition you want to add. 
Replace <BUCKET> with the name of your S3 bucket, <ACCOUNT> with the AWS account ID, <REGION> with the region, and <YEAR> with the year of the partition.

## Sample Query's

:information_source: Keep in mind you need to change the value TABLE to your tablename.

### Get Last Events of 7 days
```
SELECT * FROM TABLE
where from_iso8601_timestamp(eventtime) > date_add('day', -7, now())
```

### Get all EC2 Events
```
SELECT * FROM TABLE
where eventsource = 'ec2.amazonaws.com'
```

### Restrict Output columns
```
SELECT eventtime, eventsource, eventname, sourceipaddress, requestparameters, useridentity FROM TABLE
where eventsource = 'ec2.amazonaws.com'
```

### Check for errors in your CloudTrail logs on S3
```
SELECT * FROM TABLE
WHERE errorcode IS NOT NULL
```
### Get Login events of the last week
```
SELECT
 useridentity.username,
 sourceipaddress,
 eventtime,
 additionaleventdata
FROM TABLE
WHERE eventname = 'ConsoleLogin'
      AND eventtime >= '2019-08-23T00:00:00Z'
      AND eventtime < '2019-08-30T00:00:00Z';
```
### Get AssumeRole events of the last week
```
SELECT
 eventsource,
 eventtime,
 requestparameters,
 resources
FROM TABLE
WHERE eventname = 'AssumeRole'
      AND eventtime >= '2019-08-23T00:00:00Z'
      AND eventtime < '2019-08-30T00:00:00Z';
```