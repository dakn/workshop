ALTER TABLE cloudtrail_logs 
ADD PARTITION (account='<ACCOUNT>', region='<REGION>', year='<YEAR>') 
LOCATION 's3://<BUCKET>/AWSLogs/<ACCOUNT>/CloudTrail/<REGION>/<YEAR>/';
