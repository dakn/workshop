# Validating CloudTrail Log File Integrity with the AWS CLI

## Get Trail ARN and Bucketname
`aws cloudtrail describe-trails`
## Check Log File Integrity 
`aws cloudtrail validate-logs --trail-arn <trailARN> --start-time 2019-01-01T00:00:00Z --end-time 2019-12-31T00:00:00Z [--s3-bucket <bucket-name>] [--s3-prefix <prefix>] [--verbose]`

log files should be valid.

## Download log file
1. Login to AWS Console
2. Switch to S3
3. Select your Bucket an Download one log file eg.: 347784147225_CloudTrail_eu-central-1_20190826T0655Z_05AdNbpEHNhNDAGc.json.gz

## Delete log file
1. Login to AWS Console
2. Switch to S3
3. Select your CloudTrail Bucket and delete the same log file which you have download in the step before.
## Check log file Integrity 
`aws cloudtrail validate-logs --trail-arn <trailARN> --start-time 2019-01-01T00:00:00Z --end-time 2019-12-31T00:00:00Z [--s3-bucket <bucket-name>] [--s3-prefix <prefix>] [--verbose]`

One log file should be unvalid.

Example Output:

```
Log file	s3://test-cloudtrail-eu-central-1-347784147225/AWSLogs/347784147225/CloudTrail/eu-central-1/2019/08/26/347784147225_CloudTrail_eu-central-1_20190826T0655Z_05AdNbpEHNhNDAGc.json.gz	INVALID: invalid format

Results requested for 2019-01-01T00:00:00Z to 2019-12-31T00:00:00Z
Results found for 2019-08-26T05:53:08Z to 2019-08-27T06:53:08Z:

25/25 digest files valid
74/75 log files valid, 1/75 log files INVALID
```

## Upload log file to the Bucket again
1. Login to AWS Console
2. Switch to S3
3. Select your CloudTrail Bucket and upload the same log file which you have download in the step **Delete log file**.

## Check log file Integrity 
`aws cloudtrail validate-logs --trail-arn <trailARN> --start-time 2019-01-01T00:00:00Z --end-time 2019-12-31T00:00:00Z [--s3-bucket <bucket-name>] [--s3-prefix <prefix>] [--verbose]`

log files should be valid.