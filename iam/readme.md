# IAM MFA enforce lab

1. Create a new IAM Policy with the following settings
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowAllExceptIfMFA",
            "Effect": "Allow",
            "Action":"*",
            "Resource": "*",
            "Condition": {
                "BoolIfExists": {
                    "aws:MultiFactorAuthPresent": "true"
                }
            }
        },
        {
            "Sid": "AllowAllExceptListedIfNoMFA",
            "Effect": "Allow",
            "Action": [
                "iam:CreateVirtualMFADevice",
                "iam:EnableMFADevice",
                "iam:GetUser",
                "iam:ListUsers",
                "iam:ListMFADevices",
                "iam:ListVirtualMFADevices",
                "iam:ResyncMFADevice",
                "sts:GetSessionToken"
            ],
            "Resource": "*",
            "Condition": {
                "BoolIfExists": {
                    "aws:MultiFactorAuthPresent": "false"
                }
            }
        }
    ]
}
```
2. Create new IAM-User and attach the 